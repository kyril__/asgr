# Agsr


## List of resources:

- Helm chart
- Python web-server
- Docker dependencies

---

## Usage:
- Set your own image_name:tag in init script

### Init commands:

```
./init.sh

```
### Optional commands:
```
helm install release-name ./helm-chart
```

### Logging & monitoring schema [link](https://www.canva.com/design/DAGD4ANfP4s/23C5MgRUgZoVb36QG1uNJA/edit?utm_content=DAGD4ANfP4s&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton)

---
![image](k8s-cluster.png)
