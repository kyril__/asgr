from flask import Flask, jsonify, request 
import os 

app = Flask(__name__) 
 
@app.route('/health', methods = ['GET']) 
def health_path(): 
    if(request.method == 'GET'): 
        try:
            return jsonify(success=True),200
        except:
            return 500
  
@app.route('/message', methods = ['GET']) 
def msg_path(): 
    if (request.method == 'GET'):
        try:
            env_name = "HOSTNAME"
            config_data_name = "VARIABLE"

            env_value = os.environ.get(env_name)
            config_data_value = os.environ.get(config_data_name)

            return jsonify({'data': f"User {env_name} value is {env_value} --- Config var value is  {config_data_value}"})
        except:
            return 500
  
  
# driver function 
if __name__ == '__main__': 
    
    app.run(host = "0.0.0.0",port = 5000,debug = True) 