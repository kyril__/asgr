#!/bin/bash
echo "Build Docker image"
docker build -t ubuntucontainer350:flask_api:v4 .
echo "Push to Docker Hub"
docker push ubuntucontainer350:flask_api:v4
echo "Deploy by Helm chart"
helm install --kubeconfig=KUBECONFIG flask-release ./helm-chart

