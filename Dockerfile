FROM python:3.11
LABEL owner="kl1rik"

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . .

EXPOSE 5000
CMD [ "python3","api.py" ]
